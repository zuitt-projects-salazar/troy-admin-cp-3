// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyByuOxRU-bimMfU8ye3nQTLtnuURBI_S4Y",
  authDomain: "ecommercetroy-ff7de.firebaseapp.com",
  projectId: "ecommercetroy-ff7de",
  storageBucket: "ecommercetroy-ff7de.appspot.com",
  messagingSenderId: "508416849768",
  appId: "1:508416849768:web:64ed39241639bc3a0fc8dd",
  measurementId: "G-S12NEQ2FT2"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default app;
